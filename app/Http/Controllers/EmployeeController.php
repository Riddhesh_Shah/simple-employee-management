<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Address;
use App\Models\EmailAddress;
use App\Models\Employee;
use App\Models\MobileNumber;
use App\Models\WhatsappNumber;
use Illuminate\Support\Facades\DB;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return view('employees.index');
        $employees = Employee::all();
        return view("employees.index", compact([
            'employees'
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('employees.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $employee = Employee::create([
            'name' => $request['name']
        ]);

        $address = Address::create([
            'employee_id' => $employee->id,
            'address_1' => $request['address_1'],
            'address_2' => $request['address_2'],
            'location' => $request['location'],
            'pin_code' => $request['pin_code'],
            'postal_area' => $request['postal_area'],
            'taluka' => $request['taluka'],
            'suburb' => $request['suburb'],
            'east_west' => $request['east_west'],
            'city' => $request['city'],
            'district' => $request['district'],
            'state' => $request['state'],
            'country' => $request['country']
        ]);

        $mobileArray = $request['mobile'];
        // dd($mobileArray);
        for($i=0;$i<count($mobileArray);$i++){
            if($mobileArray[$i]['checked'] == "true"){
                $is_primary = "1";
            }else{
                $is_primary = "0";
            }
            $mobile = MobileNumber::create([
                'employee_id' => $employee->id,
                'mobile_number' => $mobileArray[$i]['value'],
                'is_primary' => $is_primary
            ]);
        }

        $whatsappArray = $request['whatsapp'];
        for($i=0;$i<count($whatsappArray);$i++){
            if($whatsappArray[$i]['checked'] == "true"){
                $is_primary = "1";
            }else{
                $is_primary = "0";
            }
            $whatsapp = WhatsappNumber::create([
                'employee_id' => $employee->id,
                'whatsapp_number' => $whatsappArray[$i]['value'],
                'is_primary' => $is_primary
            ]);
        }

        $emailArray = $request['email'];
        for($i=0;$i<count($emailArray);$i++){
            if($emailArray[$i]['checked'] == "true"){
                $is_primary = "1";
            }else{
                $is_primary = "0";
            }
            $whatsapp = EmailAddress::create([
                'employee_id' => $employee->id,
                'email_address' => $emailArray[$i]['value'],
                'is_primary' => $is_primary
            ]);
        }

        return "success";

        // DB::insert("INSERT INTO employee (name) VALUES (?)", [$request['name']]);
        // $id = DB::select("SELECT LAST_INSERT_ID()");

        // DB::insert("INSERT INTO address (employee_id, address_1, address_2, location, pin_code, postal_area, taluka, suburb, east_west, city, district, state, country) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", [$request['employee_id'], $request['address_1'], $request['address_2'], $request['location'], $request['pin_code'], $request['postal_area'], $request['taluka'], $request['suburb'], $request['east_west'], $request['city'], $request['district'], $request['state'], $request['country'],]);


        // return $request['name'];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // dd($id);
        $employee = Employee::find($id);
        $address = $employee->address;
        $mobileArray = $employee->mobileNumbers;
        $whatsappArray = $employee->whatsappNumbers;
        $emailArray = $employee->emailAddresses;
        
        return view("employees.show", compact([
            'employee',
            'address',
            'mobileArray',
            'whatsappArray',
            'emailArray',
        ]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::find($id);
        $address = $employee->address;
        $mobileArray = $employee->mobileNumbers;
        $whatsappArray = $employee->whatsappNumbers;
        $emailArray = $employee->emailAddresses;
        
        return view("employees.edit", compact([
            'employee',
            'address',
            'mobileArray',
            'whatsappArray',
            'emailArray',
        ]));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        DB::table('employees')->where('id', $id)->delete();

        $employee = Employee::create([
            'name' => $request['name']
        ]);

        $address = Address::create([
            'employee_id' => $employee->id,
            'address_1' => $request['address_1'],
            'address_2' => $request['address_2'],
            'location' => $request['location'],
            'pin_code' => $request['pin_code'],
            'postal_area' => $request['postal_area'],
            'taluka' => $request['taluka'],
            'suburb' => $request['suburb'],
            'east_west' => $request['east_west'],
            'city' => $request['city'],
            'district' => $request['district'],
            'state' => $request['state'],
            'country' => $request['country']
        ]);

        $mobileArray = $request['mobile'];
        // dd($mobileArray);
        for($i=0;$i<count($mobileArray);$i++){
            if($mobileArray[$i]['checked'] == "true"){
                $is_primary = "1";
            }else{
                $is_primary = "0";
            }
            $mobile = MobileNumber::create([
                'employee_id' => $employee->id,
                'mobile_number' => $mobileArray[$i]['value'],
                'is_primary' => $is_primary
            ]);
        }

        $whatsappArray = $request['whatsapp'];
        for($i=0;$i<count($whatsappArray);$i++){
            if($whatsappArray[$i]['checked'] == "true"){
                $is_primary = "1";
            }else{
                $is_primary = "0";
            }
            $whatsapp = WhatsappNumber::create([
                'employee_id' => $employee->id,
                'whatsapp_number' => $whatsappArray[$i]['value'],
                'is_primary' => $is_primary
            ]);
        }

        $emailArray = $request['email'];
        for($i=0;$i<count($emailArray);$i++){
            if($emailArray[$i]['checked'] == "true"){
                $is_primary = "1";
            }else{
                $is_primary = "0";
            }
            $whatsapp = EmailAddress::create([
                'employee_id' => $employee->id,
                'email_address' => $emailArray[$i]['value'],
                'is_primary' => $is_primary
            ]);
        }

        return "success";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('employees')->where('id', $id)->delete();

        return redirect(route('employees.index'));
    }
}
