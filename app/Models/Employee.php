<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;

    protected $fillable = [
        'name'
    ];

    public function address(){
        return $this->hasOne(Address::class);
    }

    public function mobileNumbers(){
        return $this->hasMany(MobileNumber::class);
    }

    public function whatsappNumbers(){
        return $this->hasMany(WhatsappNumber::class);
    }

    public function emailAddresses(){
        return $this->hasMany(EmailAddress::class);
    }
}
