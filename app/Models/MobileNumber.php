<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MobileNumber extends Model
{
    use HasFactory;

    protected $fillable = [
        'employee_id',
        'mobile_number',
        'is_primary'
    ];

    public function employee(){
        return $this->belongsTo(App\Models\Employee::class, "employee_id");
    }
}
