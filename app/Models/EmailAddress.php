<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmailAddress extends Model
{
    use HasFactory;

    protected $fillable = [
        'employee_id',
        'email_address',
        'is_primary'
    ];

    public function employee(){
        return $this->belongsTo(Employee::class);
    }

}
