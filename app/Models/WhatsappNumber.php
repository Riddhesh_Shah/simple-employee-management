<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WhatsappNumber extends Model
{
    use HasFactory;

    protected $fillable = [
        'employee_id',
        'whatsapp_number',
        'is_primary'
    ];

    public function employee(){
        return $this->belongsTo(Employee::class);
    }

}
