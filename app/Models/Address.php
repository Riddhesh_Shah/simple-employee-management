<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasFactory;

    protected $fillable = [
        'employee_id',
        'address_1',
        'address_2',
        'location',
        'pin_code',
        'postal_area',
        'taluka',
        'suburb',
        'east_west',
        'city',
        'district',
        'country',
        'state'
    ];

    public function employee(){
        return $this->belongsTo(Employee::class);
    }

}
