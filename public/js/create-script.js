function addMobileNumberCard(e){
    console.log("Worked");
    console.log($(e).parent().parent().siblings());
    var elementToBeChanged = $(e).parent().parent().siblings();
    elementToBeChanged.append(`
        <div class="mobile_number mb-3" name="mobile_number[]">
            <div class="form-group">
                <div class="d-flex justify-content-between mb-2">
                    <label class="primary-label">
                        <input type="radio" class="primary_mobile" name="primary_mobile" value="option1">
                        Primary
                    </label>
                    <div class="btn btn-danger delete-button p-1" name="delete-button[]">x</div>
                </div>
                <input type="text" class="form-control mobile" name="mobile[]" placeholder="Enter Mobile Number">
            </div>
        </div>
    `);
}

$(document).delegate(".mobile_number", "click", function(e) {
    console.log($(e.target).parent().parent().parent());
    console.log($(e.target).attr('class').split(/\s+/).includes("delete-button"))

    if($(e.target).attr('class').split(/\s+/).includes("delete-button")){
        $(e.target).parent().parent().parent().remove();
    }
});


function addWhatsappNumberCard(e){
    console.log("Worked");
    console.log($(e).parent().parent().siblings());
    var elementToBeChanged = $(e).parent().parent().siblings();
    elementToBeChanged.append(`
        <div class="whatsapp_number mb-3" name="whatsapp_number[]">
            <div class="form-group">
                <div class="d-flex justify-content-between mb-2">
                    <label class="primary-label">
                        <input type="radio" class="primary_whatsapp" name="primary_whatsapp" id="primary_whatsapp" value="option1">
                        Primary
                    </label>
                    <div class="btn btn-danger delete-button p-1" name="delete-button[]">x</div>
                </div>
                <input type="text" class="form-control whatsapp" id="whatsapp" name="whatsapp[]" placeholder="Enter Whatsapp Number">
            </div>
        </div>
    `);
}

$(document).delegate(".whatsapp_number", "click", function(e) {
    console.log($(e.target).parent().parent().parent());
    console.log($(e.target).attr('class').split(/\s+/).includes("delete-button"))

    if($(e.target).attr('class').split(/\s+/).includes("delete-button")){
        $(e.target).parent().parent().parent().remove();
    }
});

function addEmailAddressCard(e){
    console.log("Worked");
    console.log($(e).parent().parent().siblings());
    var elementToBeChanged = $(e).parent().parent().siblings();
    elementToBeChanged.append(`
        <div class="email_address mb-3" name="email_address[]">
            <div class="form-group">
                <div class="d-flex justify-content-between mb-2">
                    <label class="primary-label">
                        <input type="radio" class="primary_email" name="primary_email" id="primary_email" value="option1">
                        Primary
                    </label>
                    <div class="btn btn-danger delete-button p-1" name="delete-button[]">x</div>
                </div>
                <input type="text" class="form-control email" id="email" name="email[]" placeholder="Enter Email Address">
            </div>
        </div>
    `);
}

$(document).delegate(".email_address", "click", function(e) {
    console.log($(e.target).parent().parent().parent());
    console.log($(e.target).attr('class').split(/\s+/).includes("delete-button"))

    if($(e.target).attr('class').split(/\s+/).includes("delete-button")){
        $(e.target).parent().parent().parent().remove();
    }
});

function submissionWork(){
    console.log("reached");

    var name = $("#name")[0].value;
    var address_1 = $("#address_1")[0].value;
    var address_2 = $("#address_2")[0].value;
    var location = $("#location")[0].value;
    var pin_code = $("#pin_code")[0].value;
    var postal_area = $("#postal_area")[0].value;
    var taluka = $("#taluka")[0].value;
    var suburb = $("#suburb")[0].value;
    var east_west = $("#east_west")[0].value;
    var city = $("#city")[0].value;
    var district = $("#district")[0].value;
    var state = $("#state")[0].value;
    var country = $("#country")[0].value;

    var mobiles = $(".mobile");
    var mobileArray = [];
    for(var i=0;i<mobiles.length;i++){
        var checked = $(mobiles[i]).siblings().children().first().children().first().prop("checked");
        var value = $(mobiles[i])[0].value;
        mobileArray.push({
            value: value,
            checked: checked
        });
    }
    // console.log(mobileArray);

    var whatsapps = $(".whatsapp");
    var whatsappArray = [];
    for(var i=0;i<whatsapps.length;i++){
        var checked = $(whatsapps[i]).siblings().children().first().children().first().prop("checked");
        var value = $(whatsapps[i])[0].value;
        whatsappArray.push({
            value: value,
            checked: checked
        });
    }
    // console.log(whatsappArray);

    var emails = $(".email");
    var emailArray = [];
    for(var i=0;i<emails.length;i++){
        var checked = $(emails[i]).siblings().children().first().children().first().prop("checked");
        console.log(checked);
        var value = $(emails[i])[0].value;
        emailArray.push({
            value: value,
            checked: checked
        });
    }
    // console.log(emailArray);

    var data = {
        name: name,
        address_1: address_1,
        address_2: address_2,
        location: location,
        pin_code: pin_code,
        postal_area: postal_area,
        taluka: taluka,
        suburb: suburb,
        east_west: east_west,
        city: city,
        district: district,
        state: state,
        country: country,
        mobile: mobileArray,
        whatsapp: whatsappArray,
        email: emailArray,
        _token : $("input[name='_token']").val()
    }

    // console.log(mobileArray);

    // console.log(name + " " + address_1 + " " + address_2 + " " + location + " " + pin_code + " " + postal_area + " " + taluka + " " + suburb + " " + east_west + " " + city + " " + district + " " + state + " " + country);
    // var data 

    var baseUrl = window.origin;
    var filePath = "/employees";

    // console.log(baseUrl);

    $.ajax({
        url: baseUrl + filePath,
        method: "POST",
        data: data,
        success: function(result){
            if(result == "success"){
                window.location = baseUrl + "/employees";
            }
        }
    });
}