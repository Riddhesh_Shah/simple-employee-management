<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <title>Hello, world!</title>
  </head>
  <body>

    <div class="container mt-5 mb-5">
        <form action="" method="POST" id="employeeForm">
            @method('PATCH')
            @csrf
            <div class="card mt-5 mb-5">
                <div class="card-header">
                    <p>Employee Details</p>
                </div>
                <div class="card-body">
                    <div class="card mt-5 mb-5">
                        <div class="card-header">
                            <p>Personal Details</p>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="name" class="mb-2" >Name</label>
                                        <input type="text" class="form-control" id="name" name="name" value="<?= $employee->name?>" placeholder="Enter Name">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card mt-5 mb-5">
                        <div class="card-header">
                            Address Details
                        </div>
                        <div class="card-body">
                            <div class="row mb-3">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="address_1" class="mb-2" >Address 1</label>
                                        <input type="text" class="form-control" id="address_1" name="address_1" placeholder="Enter Address 1" value="<?= $address->address_1?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="address_2" class="mb-2" >Address 2</label>
                                        <input type="text" class="form-control" id="address_2" name="address_2" placeholder="Enter Address 2" value="<?= $address->address_2?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="location" class="mb-2" >Location</label>
                                        <input type="text" class="form-control" id="location" name="location" placeholder="Enter Location" value="<?= $address->location?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="pin_code" class="mb-2" >Pin Code</label>
                                        <input type="text" class="form-control" id="pin_code" name="pin_code" placeholder="Enter Pin Code" value="<?= $address->pin_code?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="postal_area" class="mb-2" >Postal Area</label>
                                        <input type="text" class="form-control" id="postal_area" name="postal_area" placeholder="Enter Postal Area" value="<?= $address->postal_area?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="taluka" class="mb-2" >Taluka</label>
                                        <input type="text" class="form-control" id="taluka" name="taluka" placeholder="Enter Taluka" value="<?= $address->taluka?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="suburb" class="mb-2" >Suburb</label>
                                        <input type="text" class="form-control" id="suburb" name="suburb" placeholder="Enter Suburb" value="<?= $address->suburb?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="east_west" class="mb-2" >East / West</label>
                                        <input type="text" class="form-control" id="east_west" name="east_west" placeholder="Enter East / West" value="<?= $address->east_west?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="loccityation" class="mb-2" >City</label>
                                        <input type="text" class="form-control" id="city" name="city" placeholder="Enter City" value="<?= $address->city?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="district" class="mb-2" >District</label>
                                        <input type="text" class="form-control" id="district" name="district" placeholder="Enter District" value="<?= $address->district?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="state" class="mb-2" >State</label>
                                        <input type="text" class="form-control" id="state" name="state" placeholder="Enter State" value="<?= $address->state?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="country" class="mb-2" >Country</label>
                                        <input type="text" class="form-control" id="country" name="country" placeholder="Enter Country" value="<?= $address->country?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card mt-5 mb-5">
                        <div class="card-header">
                            Contact Details
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="card" id="mobile-card">
                                        <div class="card-header">
                                            <div class="d-flex justify-content-between align-middle">
                                                <p class="m-0">Mobile Number</p>
                                                <div class="btn btn-primary add-button p-1" id="add-mobile-number-btn" onclick="addMobileNumberCard(this)">+</div>
                                            </div>
                                        </div>
                                        <div class="card-body">
@for ($i=0;$i<count($mobileArray);$i++)
<?php
    $mobile = $mobileArray[$i]
?>
@if($i==0)

                                            <div class="mobile_number mb-3" name="mobile_number[]">
                                                <div class="form-group">
                                                    <div class="d-flex justify-content-between mb-2">
                                                        <label class="primary-label">
                                                            <input type="radio" class="primary_mobile" name="primary_mobile" id="primary_mobile" value="option1" {{$mobile->is_primary == "1" ? "checked=''" : "" }}>
                                                            Primary
                                                        </label>
                                                    </div>
                                                    <input type="text" class="form-control mobile" name="mobile[]" placeholder="Enter Mobile Number" value="<?= $mobile->mobile_number?>">
                                                </div>
                                            </div>
@else
                                            <div class="mobile_number mb-3" name="mobile_number[]">
                                                <div class="form-group">
                                                    <div class="d-flex justify-content-between mb-2">
                                                        <label class="primary-label">
                                                            <input type="radio" class="primary_mobile" name="primary_mobile" id="primary_mobile" value="option1" {{$mobile->is_primary == "1" ? "checked=''" : "" }}>
                                                            Primary
                                                        </label>
                                                        <div class="btn btn-danger delete-button p-1" name="delete-button[]">x</div>
                                                    </div>
                                                    <input type="text" class="form-control mobile" name="mobile[]" placeholder="Enter Mobile Number" value="<?= $mobile->mobile_number?>">
                                                </div>
                                            </div>
@endif
@endfor
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card" id="whatsapp-card">
                                        <div class="card-header">
                                            <div class="d-flex justify-content-between align-middle">
                                                <p class="m-0">Whatsapp Number</p>
                                                <div class="btn btn-primary add-button p-1" id="add-whatsapp-number-btn" onclick="addWhatsappNumberCard(this)">+</div>
                                            </div>
                                        </div>
                                        <div class="card-body">
@for ($i=0;$i<count($whatsappArray);$i++)
<?php
    $whatsapp = $whatsappArray[$i]
?>
@if($i==0)
                                            <div class="whatsapp_number mb-3" name="whatsapp_number[]">
                                                <div class="form-group">
                                                    <div class="d-flex justify-content-between mb-2">
                                                        <label class="primary-label">
                                                            <input type="radio" class="primary_whatsapp" name="primary_whatsapp" id="primary_whatsapp" value="option1" {{$whatsapp->is_primary == "1" ? "checked=''" : "" }}>
                                                            Primary
                                                        </label>
                                                    </div>
                                                    <input type="text" class="form-control whatsapp" id="whatsapp" name="whatsapp[]" placeholder="Enter Whatsapp Number" value="<?= $whatsapp->whatsapp_number?>">
                                                </div>
                                            </div>
@else
                                            <div class="whatsapp_number mb-3" name="whatsapp_number[]">
                                                <div class="form-group">
                                                    <div class="d-flex justify-content-between mb-2">
                                                        <label class="primary-label">
                                                            <input type="radio" class="primary_whatsapp" name="primary_whatsapp" id="primary_whatsapp" value="option1" {{$whatsapp->is_primary == "1" ? "checked=''" : "" }}>
                                                            Primary
                                                        </label>
                                                        <div class="btn btn-danger delete-button p-1" name="delete-button[]">x</div>
                                                    </div>
                                                    <input type="text" class="form-control whatsapp" id="whatsapp" name="whatsapp[]" placeholder="Enter Whatsapp Number" value="<?= $whatsapp->whatsapp_number?>">
                                                </div>
                                            </div>
@endif
@endfor
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card" id="email-card">
                                        <div class="card-header">
                                            <div class="d-flex justify-content-between align-middle">
                                                <p class="m-0">Email Address</p>
                                                <div class="btn btn-primary add-button p-1" id="add-email-address-btn" onclick="addEmailAddressCard(this)">+</div>
                                            </div>
                                        </div>
                                        <div class="card-body">
@for ($i=0;$i<count($emailArray);$i++)
<?php
    $email = $emailArray[$i]
?>
@if($i==0)
                                            <div class="email_address mb-3" name="email_address[]">
                                                <div class="form-group">
                                                    <div class="d-flex justify-content-between mb-2">
                                                        <label class="primary-label">
                                                            <input type="radio" class="primary_email" name="primary_email" id="primary_email" value="option1"  {{$email->is_primary == "1" ? "checked=''" : "" }}>
                                                            Primary
                                                        </label>
                                                    </div>
                                                    <input type="text" class="form-control email" id="email" name="email[]" placeholder="Enter Email Address" value="<?= $email->email_address?>">
                                                </div>
                                            </div>
@else
                                            <div class="email_address mb-3" name="email_address[]">
                                                <div class="form-group">
                                                    <div class="d-flex justify-content-between mb-2">
                                                        <label class="primary-label">
                                                            <input type="radio" class="primary_email" name="primary_email" id="primary_email" value="option1"  {{$email->is_primary == "1" ? "checked=''" : "" }}>
                                                            Primary
                                                        </label>
                                                        <div class="btn btn-danger delete-button p-1" name="delete-button[]">x</div>
                                                    </div>
                                                    <input type="text" class="form-control email" id="email" name="email[]" placeholder="Enter Email Address" value="<?= $email->email_address?>">
                                                </div>
                                            </div>
@endif
@endfor
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-end">
                    {{-- <div class="btn btn-primary" onclick="$('#employeeForm').submit()">Submit</div> --}}
                    <div class="btn btn-primary" onclick="submissionWork()">Submit</div>
                </div>
            </div>
        </form>
    </div>

    {{-- <form action="index.php" method="GET" id="myForm">
        <div class="btn btn-primary" type="" onclick="$('#myForm').submit()">Submit</div>
    </form> --}}

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>

    <!-- <script>

        function addMobileNumberCard(e){
            console.log("Worked");
            console.log($(e).parent().parent().siblings());
            var elementToBeChanged = $(e).parent().parent().siblings();
            elementToBeChanged.append(`
                <div class="mobile_number mb-3" name="mobile_number[]">
                    <div class="form-group">
                        <div class="d-flex justify-content-between mb-2">
                            <label class="primary-label">
                                <input type="radio" class="primary_mobile" name="primary_mobile" value="option1">
                                Primary
                            </label>
                            <div class="btn btn-danger delete-button p-1" name="delete-button[]">x</div>
                        </div>
                        <input type="text" class="form-control" name="mobile[]" placeholder="Enter Mobile Number">
                    </div>
                </div>
            `);
        }

        $(document).delegate(".mobile_number", "click", function(e) {
            console.log($(e.target).parent().parent().parent());
            console.log($(e.target).attr('class').split(/\s+/).includes("delete-button"))

            if($(e.target).attr('class').split(/\s+/).includes("delete-button")){
                $(e.target).parent().parent().parent().remove();
            }
        });


        function addWhatsappNumberCard(e){
            console.log("Worked");
            console.log($(e).parent().parent().siblings());
            var elementToBeChanged = $(e).parent().parent().siblings();
            elementToBeChanged.append(`
                <div class="whatsapp_number mb-3" name="whatsapp_number[]">
                    <div class="form-group">
                        <div class="d-flex justify-content-between mb-2">
                            <label class="primary-label">
                                <input type="radio" class="primary_whatsapp" name="primary_whatsapp" id="primary_whatsapp" value="option1">
                                Primary
                            </label>
                            <div class="btn btn-danger delete-button p-1" name="delete-button[]">x</div>
                        </div>
                        <input type="text" class="form-control" id="whatsapp" name="whatsapp[]" placeholder="Enter Whatsapp Number">
                    </div>
                </div>
            `);
        }

        $(document).delegate(".whatsapp_number", "click", function(e) {
            console.log($(e.target).parent().parent().parent());
            console.log($(e.target).attr('class').split(/\s+/).includes("delete-button"))

            if($(e.target).attr('class').split(/\s+/).includes("delete-button")){
                $(e.target).parent().parent().parent().remove();
            }
        });

        function addEmailAddressCard(e){
            console.log("Worked");
            console.log($(e).parent().parent().siblings());
            var elementToBeChanged = $(e).parent().parent().siblings();
            elementToBeChanged.append(`
                <div class="email_address mb-3" name="email_address[]">
                    <div class="form-group">
                        <div class="d-flex justify-content-between mb-2">
                            <label class="primary-label">
                                <input type="radio" class="primary_email" name="primary_email" id="primary_email" value="option1" checked="">
                                Primary
                            </label>
                            <div class="btn btn-danger delete-button p-1" name="delete-button[]">x</div>
                        </div>
                        <input type="text" class="form-control" id="email" name="email[]" placeholder="Enter Email Address">
                    </div>
                </div>
            `);
        }

        $(document).delegate(".email_address", "click", function(e) {
            console.log($(e.target).parent().parent().parent());
            console.log($(e.target).attr('class').split(/\s+/).includes("delete-button"))

            if($(e.target).attr('class').split(/\s+/).includes("delete-button")){
                $(e.target).parent().parent().parent().remove();
            }
        });


    </script> -->

    <script>var id = {{ $employee->id }}</script>
    <script src="{{ asset('js/edit-script.js') }}" ></script>

    {{-- 
        Radio can have value
        Form submits only selected radio value
        So now to set the value as the data in input field using onchange
        --}}

  </body>
</html>