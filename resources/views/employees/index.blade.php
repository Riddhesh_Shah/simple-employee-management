<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" integrity="sha512-5A8nwdMOWrSz20fDsjczgUidUBR8liPYU+WymTZP1lmY9G6Oc7HlZv156XqnsgNUzTyMefFTcsFH/tnJE/+xBg==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <title>Hello, world!</title>
  </head>
  <body>
      <div class="container mt-5">

        <table id="employee_table" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>Sr No.</th>
                    <th>Name</th>
                    <th>Operation</th>
                </tr>
            </thead>
            <tbody>
@foreach ($employees as $employee)
                <tr>
                    <td></td>
                    <td><?= $employee->name ?></td>
                    <td>
                        <div class="view-card d-inline-block mr-2">
                            <form action="{{ route('employees.show',$employee->id) }}">
                                <button class="btn btn-secondary view-btn" type="submit">
                                    <div class="fa fa-eye"></div>
                                </button>
                            </form>
                        </div>
                        <div class="edit-card d-inline-block mr-2">
                            <form action="{{ route('employees.edit',$employee->id) }}">
                                <button class="btn btn-primary edit-btn" type="submit">
                                    <div class="fa fa-pencil"></div>
                                </button>
                            </form>
                        </div>
                        <div class="delete-card d-inline-block mr-2">
                            <form action="{{ route('employees.destroy',$employee->id) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger delete-btn" type="submit">
                                    <div class="fa fa-trash"></div>
                                </button>
                            </form>
                        </div>
                    </td>
                </tr>
@endforeach
            </tbody>
        </table>

      </div>
  </body>

    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
    <script>
    $(document).ready(function() {
        $('#employee_table').DataTable({
            "fnRowCallback" : function(nRow, aData, iDisplayIndex){
                $("td:first", nRow).html(iDisplayIndex +1);
                return nRow;
        },
        'columnDefs': [ {
            'targets': [2], // column index (start from 0)
            'orderable': false, // set orderable false for selected columns
        }]
        });
    } );
    </script>
</html>